import React from "react";
import File from "../../types/File";
import FilesList from "../FilesList";
import Pagination from "../Pagination";

const styles = {
  filesListContainer: {
    marginBottom: ".5rem"
  }
};

type FilesProps = {
  files: File[];
  loading: boolean;
  currentPage: number;
  totalFiles: number;
  handlePageChange: (page: number) => void;
};

const Files: React.FC<FilesProps> = ({
  files,
  loading,
  currentPage,
  totalFiles,
  handlePageChange
}) => {
  return (
    <>
      <FilesList
        files={files}
        loading={loading}
        style={styles.filesListContainer}
      />
      {!loading && (
        <Pagination
          currentPage={currentPage}
          totalItems={totalFiles}
          onPageChange={handlePageChange}
          itemsPerPage={10}
        />
      )}
    </>
  );
};

export default Files;
