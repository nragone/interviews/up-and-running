import React from "react";
import { FaFile } from "react-icons/fa";
import FileType from "../../types/File";
import { Link } from "react-router-dom";

import "./style.scss";

export type FileProps = {
  file: FileType;
};

const File: React.FC<FileProps> = ({ file }: FileProps) => {
  return (
    <div className="file-component">
      <FaFile size="2rem" />
      <Link
        to={{
          pathname: `/edit/${file.id}`,
          state: { file }
        }}
      >
        {file.name}
      </Link>
    </div>
  );
};

export default File;
