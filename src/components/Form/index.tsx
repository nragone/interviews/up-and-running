import React, { InputHTMLAttributes } from "react";
import Button from "./Button";
import Input from "./Input";
import Message from "../Message";

import "./style.scss";
import Loader from "../Loader";

type FormActionsProps = {};

const FormActions: React.FC<FormActionsProps> = ({ children }) => {
  return <div className="form-actions">{children}</div>;
};

type FormProps = {
  onSubmit: any;
  error?: boolean;
  message?: string;
  loading?: boolean;
};

type FormState = {
  fields: { [key: string]: any };
  errors: { [key: string]: string };
};

class Form extends React.Component<FormProps, FormState> {
  static Input: typeof Input = Input;
  static Button: typeof Button = Button;

  static Actions: typeof FormActions = FormActions;

  constructor(props: any) {
    super(props);

    // Get child fields and add those to the state
    const fields = React.Children.map(this.props.children, (child: any) => {
      return {
        name: child.props.name,
        value: child.props.value,
        validation: child.props.validation
      };
    });

    const reduced = fields.reduce((acc, field) => {
      if (!field.name) return acc;
      return {
        ...acc,
        [field.name]: {
          name: field.name,
          value: field.value,
          validation: field.validation
        }
      };
    }, {});

    this.state = {
      fields: reduced,
      errors: {}
    };
  }

  validate = () => {
    let errors: FormState["errors"] = {};

    Object.values(this.state.fields).map((field: FormState["fields"]) => {
      if (field.validation) {
        const result = field.validation(field.value);

        if (!result.valid) {
          errors[field.name] = result.error;
        }
      }
    });
    return errors;
  };

  handleFieldChange = ({
    target: { name, value }
  }: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      fields: {
        [name]: {
          ...this.state.fields[name],
          value: value
        }
      }
    });
  };

  handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const errors = this.validate();

    if (!Object.keys(errors).length) {
      this.props.onSubmit(this.state.fields);
    }

    this.setState({ errors });
  };

  render() {
    const { fields, errors } = this.state;
    const { error, message, loading } = this.props;

    return (
      <form onSubmit={this.handleSubmit} className="form-component">
        {React.Children.map(this.props.children, (child: any) => {
          return React.cloneElement(child, {
            onChange: this.handleFieldChange,
            value: fields[child.props.name]
              ? fields[child.props.name].value
              : "",
            error: errors[child.props.name] ? errors[child.props.name] : ""
          });
        })}
        {loading && <Loader />}
        {message && (
          <Message
            className="form-message"
            content={message}
            color={error ? "danger" : "positive"}
          />
        )}
      </form>
    );
  }
}

export default Form;
