import React, { ButtonHTMLAttributes } from "react";

type ButtonProps = {
  className?: string;
};

const Button: React.FC<ButtonProps & ButtonHTMLAttributes<any>> = ({
  className = "",
  ...rest
}) => <button className={`form-button ${className}`} {...rest} />;

export default Button;
