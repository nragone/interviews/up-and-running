import React, { InputHTMLAttributes } from "react";

type InputProps = {
  className?: string;
  label?: string;
  error?: string;
  validation?: (value: any) => { valid: boolean; error: string };
};

const Input: React.FC<InputProps & InputHTMLAttributes<HTMLInputElement>> = ({
  className = "",
  label,
  name,
  error,
  validation,
  ...rest
}) => {
  return (
    <div className="form-field">
      {label && (
        <label className="form-label" htmlFor={name}>
          {label}
        </label>
      )}
      <input className={`form-input ${className}`} name={name} {...rest} />
      <div className="input-error">{error}</div>
    </div>
  );
};

export default Input;
