import React from "react";
import TagType from "../../types/Tag";

import "./style.scss";
import Tag from "../../types/Tag";
import Loader from "../Loader";

type TagsListProps = {
  tags: Tag[];
  onTagClick: any;
  selectedTag: string;
  loading?: boolean;
};

const TagsList: React.FC<TagsListProps> = ({
  tags,
  onTagClick,
  selectedTag,
  loading = false
}) => {
  return (
    <div className="tags-list-component">
      <div
        className={`tag ${!selectedTag ? "selected" : ""}`}
        onClick={onTagClick()}
      >
        All
      </div>
      {loading && <Loader />}
      {tags.map((tag: TagType) => (
        <div
          key={tag.tag}
          className={`tag ${selectedTag == tag.tag ? "selected" : ""}`}
          onClick={onTagClick(tag)}
        >
          {tag.tag} ({tag.files})
        </div>
      ))}
    </div>
  );
};

export default TagsList;
