import React from "react";
import { FaSpinner } from "react-icons/fa";

import "./style.scss";

type LoaderProps = {
  show?: boolean;
};

const Loader: React.FC<LoaderProps> = ({ show = true }) => {
  if (!show) return null;

  return (
    <div className="loader-component">
      <FaSpinner className="rotating" />
    </div>
  );
};

export default Loader;
