import React from "react";

const PaginationButton: React.FC<{ selected?: boolean; onClick: any }> = ({
  selected = false,
  onClick,
  children
}) => {
  return (
    <div
      className={`page-button ${selected ? "selected" : ""}`}
      onClick={!selected ? onClick : null}
    >
      {children}
    </div>
  );
};

export default PaginationButton;
