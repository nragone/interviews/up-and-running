import React, { EventHandler } from "react";
import { range } from "./helpers";

import PaginationButton from "./PaginationButton";

import "./style.scss";

const LEFT_PAGE = "LEFT";
const RIGHT_PAGE = "RIGHT";

type PaginationProps = {
  currentPage: number;
  totalItems: number;
  itemsPerPage: number;
  pageNeighbours?: number;
  onPageChange: (page: number) => void;
};

class Pagination extends React.Component<PaginationProps> {
  private totalPages: number;
  private pageNeighbours: number;

  constructor(props: PaginationProps) {
    super(props);

    const { pageNeighbours } = props;

    this.pageNeighbours = pageNeighbours
      ? Math.max(0, Math.min(pageNeighbours, 2))
      : 0;

    this.totalPages = this.calcPages();
  }

  calcPages = () => {
    this.totalPages = Math.ceil(
      this.props.totalItems / this.props.itemsPerPage
    );

    return this.totalPages;
  };

  fetchPageNumbers = () => {
    const totalPages = this.totalPages;
    const currentPage = this.props.currentPage;
    const pageNeighbours = this.pageNeighbours;

    /**
     * totalNumbers: the total page numbers to show on the control
     * totalBlocks: totalNumbers + 2 to cover for the left(<) and right(>) controls
     */
    const totalNumbers = this.pageNeighbours * 2 + 3;
    const totalBlocks = totalNumbers + 2;

    if (totalPages > totalBlocks) {
      const startPage = Math.max(2, currentPage - pageNeighbours);
      const endPage = Math.min(totalPages - 1, currentPage + pageNeighbours);

      let pages: Array<any> = range(startPage, endPage);

      /**
       * hasLeft: hidden pages to the left?
       * hasRight: hidden pages to the right?
       * offset: number of hidden pages either to the left or to the right
       */
      const hasLeft = startPage > 2;
      const hasRight = totalPages - endPage > 1;
      const offset = totalNumbers - (pages.length + 1);

      switch (true) {
        // handle: (1) < {5 6} [7] {8 9} (10)
        case hasLeft && !hasRight: {
          const extraPages = range(startPage - offset, startPage - 1);
          pages = [LEFT_PAGE, ...extraPages, ...pages];
          break;
        }

        // handle: (1) {2 3} [4] {5 6} > (10)
        case !hasLeft && hasRight: {
          const extraPages = range(endPage + 1, endPage + offset);
          pages = [...pages, ...extraPages, RIGHT_PAGE];
          break;
        }

        // handle: (1) < {4 5} [6] {7 8} > (10)
        case hasLeft && hasRight:
        default: {
          pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
          break;
        }
      }

      return [1, ...pages, totalPages];
    }

    return range(1, totalPages);
  };

  gotoPage = (page: number) => {
    const currentPage = Math.max(0, Math.min(page, this.totalPages));
    this.props.onPageChange(currentPage);
  };

  handleClick = (page: number) => (evt: any) => {
    evt.preventDefault();
    this.gotoPage(page);
  };

  handleMoveLeft = (evt: any) => {
    evt.preventDefault();
    this.gotoPage(this.props.currentPage - this.pageNeighbours * 2 - 1);
  };

  handleMoveRight = (evt: any) => {
    evt.preventDefault();
    this.gotoPage(this.props.currentPage + this.pageNeighbours * 2 + 1);
  };

  render() {
    if (!this.props.totalItems || this.totalPages === 1) return null;

    const { currentPage } = this.props;
    const pages = this.fetchPageNumbers();

    return (
      <>
        <div className="pagination-component">
          {pages.map((page, index) => {
            if (page === LEFT_PAGE)
              return (
                <PaginationButton key={index} onClick={this.handleMoveLeft}>
                  <span>&laquo;</span>
                </PaginationButton>
              );

            if (page === RIGHT_PAGE)
              return (
                <PaginationButton key={index} onClick={this.handleMoveRight}>
                  <span>&raquo;</span>
                </PaginationButton>
              );

            return (
              <PaginationButton
                key={index}
                onClick={this.handleClick(page)}
                selected={currentPage === page}
              >
                {page}
              </PaginationButton>
            );
          })}
        </div>
      </>
    );
  }
}

export default Pagination;
