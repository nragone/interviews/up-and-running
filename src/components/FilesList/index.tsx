import React, { AllHTMLAttributes } from "react";
import FileType from "../../types/File";
import File from "../File";

import "./style.scss";
import Loader from "../Loader";

export type FileListProps = {
  files: FileType[];
  loading?: boolean;
};

const FilesList: React.FC<
  FileListProps & AllHTMLAttributes<HTMLDivElement>
> = ({ files, loading = false, className = "", ...rest }) => {
  return (
    <div className={`files-list-component ${className}`} {...rest}>
      {loading ? (
        <Loader />
      ) : files.length ? (
        files.map((file, index) => (
          <div key={`${index}-${file.name}`} className="file">
            <File file={file} />
          </div>
        ))
      ) : (
        <div>No Files</div>
      )}
    </div>
  );
};

export default FilesList;
