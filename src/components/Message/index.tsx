import React from "react";

import "./style.scss";

type MessageProps = {
  className?: string;
  title?: string;
  content?: string;
  color?: "warning" | "positive" | "danger";
};

const Message: React.FC<MessageProps> = ({
  className = "",
  title,
  content,
  color
}) => {
  return (
    <div className={`message-component ${color || "positive"} ${className}`}>
      {title && <div className="title">{title}</div>}
      <p className="content">{content}</p>
    </div>
  );
};

export default Message;
