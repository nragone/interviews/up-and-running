import React from "react";
import TagType from "../../types/Tag";

import "./style.scss";

type SideMenuProps = {};
const SideMenu: React.FC<SideMenuProps> = ({ children }) => {
  return (
    <nav role="navigation" className="side-menu-component">
      <input type="checkbox" id="side" name="" value="" />
      <label htmlFor="side" className="toggle" />
      <div className="sidebar">
        <div className="sidebar-wrapper">{children}</div>
      </div>
    </nav>
  );
};

export default SideMenu;
