import axios from "axios";
import config from "../config";
import Tag from "../types/Tag";

export async function getTags(): Promise<Tag[]> {
  return axios
    .get(config.endpoints.tags)
    .then(res => {
      if (res.data) {
        return res.data;
      }
      return [];
    })
    .catch(e => {
      console.log("Error getting tags", e);
      return [];
    });
}
