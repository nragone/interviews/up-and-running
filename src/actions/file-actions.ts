import axios from "axios";
import config from "../config";
import File from "../types/File";

type AsyncProps = {
  url: string;
  error?: boolean;
  message?: string;
};

type GetFilesResponse = {
  page: string;
  files: File[];
  total: number;
} & AsyncProps;

export async function getFiles(
  page: string,
  tagName?: string
): Promise<GetFilesResponse> {
  console.log("getFiles", page, tagName);
  const url = config.endpoints.files.list(page, tagName ? tagName : "");
  return axios
    .get(url)
    .then(res => {
      if (res.data) {
        return {
          page: page,
          files: res.data.files,
          total: res.data.total_files,
          url
        } as GetFilesResponse;
      }
      return {
        page: "1",
        files: [],
        total: 0,
        url
      } as GetFilesResponse;
    })
    .catch(e => {
      console.log("Error getting files", e);
      return {
        page: "1",
        files: [],
        total: 0,
        url,
        error: true,
        message: `Error getting files`
      };
    });
}

type UpdateFilenameResponse = {
  id: string;
  filename: string;
} & AsyncProps;

export async function updateFilename(
  id: string,
  filename: string
): Promise<UpdateFilenameResponse> {
  console.log("updateFilename", id, filename);
  const url = config.endpoints.files.update(id);

  return axios
    .post(url, { filename })
    .then(res => {
      if (res.data) {
        console.log("Updated!", res);
        return {
          id,
          filename,
          url,
          error: false,
          message: res.data.message
        } as UpdateFilenameResponse;
      }
      return {
        id,
        filename,
        url,
        error: true,
        message: "Error updating file"
      } as UpdateFilenameResponse;
    })
    .catch(e => {
      console.log("Error updating file", e);
      return {
        id,
        filename,
        url,
        error: true,
        message: `Error updating file`
      };
    });
}
