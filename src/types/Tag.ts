type Tag = {
  tag: string;
  files: string;
};

export default Tag;
