const baseUrl = "https://tim.uardev.com/trial-project/api";

export default {
  endpoints: {
    tags: `${baseUrl}/tags`,
    files: {
      update: (id: string) => `${baseUrl}/file/${id}/rename`,
      list: (page: string, tag: string) =>
        `${baseUrl}/files?page=${page}${tag ? `&tag=${tag}` : ""}`
    }
  }
};
