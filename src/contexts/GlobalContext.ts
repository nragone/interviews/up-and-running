import React from "react";

import File from "../types/File";
import Tag from "../types/Tag";

export interface GlobalContextState {
  files: File[];
  tags: Tag[];
  currentPage: string;
  totalFiles: number;
  selectedTag?: string;
  loadingFiles: boolean;
  loadingTags: boolean;
}

export default React.createContext<GlobalContextState>({
  files: [],
  tags: [],
  selectedTag: "",
  currentPage: "1",
  totalFiles: 0,
  loadingFiles: false,
  loadingTags: false
});
