import React from "react";

import SideMenu from "../components/SideMenu";
import TagsList from "../components/TagsList";

import GlobalContext from "../contexts/GlobalContext";
import Tag from "../types/Tag";
import Files from "../components/Files";

class Home extends React.Component<any> {
  static contextType = GlobalContext;

  handlePageChange = (page: number) => {
    this.props.history.push({
      pathname: "/files",
      search: `?page=${page}${
        this.context.selectedTag ? `&tag=${this.context.selectedTag}` : ""
      }`
    });
    this.context.getPage(page);
  };

  handleTagChange = (tag?: Tag) => () => {
    this.props.history.push({
      pathname: "/files",
      search: `?page=1${tag ? `&tag=${tag.tag}` : ""}`
    });
    this.context.getFiles("1", tag ? tag.tag : "");
  };

  render() {
    const {
      tags,
      files,
      selectedTag,
      currentPage,
      totalFiles,
      loadingFiles,
      loadingTags
    } = this.context;

    return (
      <>
        <div className="side-menu">
          <SideMenu>
            <TagsList
              tags={tags}
              onTagClick={this.handleTagChange}
              selectedTag={selectedTag}
              loading={loadingTags}
            />
          </SideMenu>
        </div>
        <main className="content">
          <section className="section">
            <header className="header">
              <h3>Search Results - "{selectedTag ? selectedTag : "All"}"</h3>
            </header>
            <div className="files">
              <Files
                files={files}
                loading={loadingFiles}
                currentPage={parseInt(currentPage)}
                totalFiles={totalFiles}
                handlePageChange={this.handlePageChange}
              />
            </div>
          </section>
        </main>
      </>
    );
  }
}

export default Home;
