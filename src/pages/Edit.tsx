import React from "react";
import GlobalContext from "../contexts/GlobalContext";
import Form from "../components/Form";
import { Redirect } from "react-router";

type EditState = {
  error: boolean;
  message: string;
  loading: boolean;
};

class Edit extends React.Component<any, EditState> {
  static contextType = GlobalContext;

  state: EditState = {
    error: false,
    message: "",
    loading: false
  };

  handleFormSubmit = (fields: any) => {
    this.setState(
      {
        loading: true
      },
      () => {
        this.context
          .updateFilename(
            this.props.location.state.file.id,
            fields.filename.value
          )
          .then((res: any) => {
            this.setState(
              {
                error: res.error,
                message: res.message,
                loading: false
              },
              () => {
                this.props.history.replace({
                  state: {
                    file: {
                      ...this.props.location.state.file,
                      name: res.filename
                    }
                  }
                });
              }
            );
          });
      }
    );
  };

  filenameValidation = (value: string) => {
    if (!value) return { valid: false, error: "Cannot be empty" };
    return { valid: true, error: "" };
  };

  render() {
    if (!this.props.location.state) return <Redirect to="/" />;
    const { file } = this.props.location.state;

    return (
      <main className="content">
        <section className="section centered">
          <header className="header">
            <h2>Rename File</h2>
          </header>
          <Form
            onSubmit={this.handleFormSubmit}
            error={this.state.error}
            message={this.state.message}
            loading={this.state.loading}
          >
            <Form.Input
              name="filename"
              type="text"
              value={file ? file.name : ""}
              label="Filename"
              validation={this.filenameValidation}
            />
            <Form.Actions>
              <Form.Button type="button" onClick={this.props.history.goBack}>
                Back to File List
              </Form.Button>
              <Form.Button>Save</Form.Button>
            </Form.Actions>
          </Form>
        </section>
      </main>
    );
  }
}

export default Edit;
