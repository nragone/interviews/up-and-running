import React, { Component } from "react";
import queryString from "query-string";
import { Switch, Route, Redirect, withRouter } from "react-router-dom";

import GlobalContext, { GlobalContextState } from "./contexts/GlobalContext";
import * as actions from "./actions";

// PAGES
import Home from "./pages/Home";
import Edit from "./pages/Edit";

import "./App.scss";

const initialState: GlobalContextState = {
  files: [],
  tags: [],
  selectedTag: "",
  currentPage: "1",
  totalFiles: 0,
  loadingFiles: false,
  loadingTags: false
};

class App extends Component<any, GlobalContextState> {
  state: GlobalContextState = initialState;

  componentDidMount() {
    const query = queryString.parse(this.props.location.search);
    // Load all tags
    this.getTags().then(() => {
      // Load files based on query params if present
      this.getFiles(
        query.page ? (query.page as string) : "1",
        query.tag ? (query.tag as string) : ""
      );
    });
  }

  /**
   *  These methods are managing the backend calls and state updates
   */
  getTags = async () => {
    this.setState(
      {
        loadingTags: true
      },
      () => {
        return actions.getTags().then(res => {
          this.setState({ tags: res, loadingTags: false });
        });
      }
    );
  };

  getFiles = async (page: string, tagName?: string) => {
    // First update selected tag for better UI experience
    this.setState(
      {
        selectedTag: tagName,
        loadingFiles: true
      },
      () => {
        return actions.getFiles(page, tagName).then(res => {
          this.setState({
            currentPage: res.page,
            files: res.files,
            totalFiles: res.total,
            loadingFiles: false
          });
        });
      }
    );
  };

  getPage = (page: string) => {
    this.getFiles(page, this.state.selectedTag);
  };

  updateFilename = async (id: string, filename: string) => {
    return actions.updateFilename(id, filename).then(res => {
      if (!res.error) {
        const updatedFiles = this.state.files.map(file => {
          if (file.id === res.id) {
            file.name = res.filename;
          }
          return file;
        });

        this.setState({
          files: updatedFiles
        });
      }

      return res;
    });
  };

  render() {
    return (
      <GlobalContext.Provider
        value={{
          ...this.state,
          getTags: this.getTags,
          getFiles: this.getFiles,
          getPage: this.getPage,
          updateFilename: this.updateFilename
        }}
      >
        <div className="app">
          <Switch>
            <Route path="/edit/:file" component={Edit} />
            <Route path="/files" component={Home} />
            <Redirect to="/files" />
          </Switch>
        </div>
      </GlobalContext.Provider>
    );
  }
}

export default withRouter(App);
