# Up and Running Interview Test

Web-based application that allows users to search through files using tags attached to
those files.

Live demo: https://up-and-running-interview-git-master.nragone.now.sh

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development. See deployment for notes on how to deploy the project on a live system.

### Install and Run

Install dependencies

```
npm install
```

Run local development environment

```
npm start
```

## Deployment

Just push the code and it will be automatically deployed through Gitlab using Zeit.co

If you push to the master branch it will be automatically deployed under the domain defined in the now.json file (not working currently because of certificate generation in zeit.co)

If you push it to some other branch it will be deployed under the following domain
https://up-and-running-interview-git-[branchname].nragone.now.sh

## Decisions made

### Why don't use Redux:

For such a small project I think that Redux adds too much complexity. Instead of using that I decided to implement it using the Context API that React provides and they released as stable since version 16.3.

Of course, this is not an optimal and scalable solution, we could, for example, create a HOC in order to inject the context properties into the components that we want to connect and also add mappers for the state and the actions just as Redux does with connect, mapStateToProps and mapDispatchToProps.

## Development times

- Setting up react app with typescript support and sass, GitLab repository and Zeit deployment configuration: ~1 hr

- Context definition and main skeleton for markup: ~1 hr.

- Routing definition: ~0.5 hr

- Presentational components like Lists, Message, Loader: a few minutes each

Smart components:

- Pagination: ~2 hrs.
- Form with validation: ~3 hrs.

- Side menu component (This is just a css implementation, no javascript involved): ~1 hr.

- Actions to get files and tags and update files: ~1hr.

These are estimated times, I'm not used to tracking exactly the hours for each task. You always have some unforeseen situations or improvements on the fly while developing.

## Built With

- [Create React App](https://facebook.github.io/create-react-app/)
- [React](https://reactjs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [React Router](https://reacttraining.com/react-router/web/guides/quick-start)
- [React Icons](https://react-icons.netlify.com/#/)
- [Sass](https://sass-lang.com/)
- [Axios](https://github.com/axios/axios)

## Authors

- **Nicolas Ragone** - [NRagone](https://www.nragone.com)

## Improvements

1. Endpoint to get specific file (GET: /files/id) in order to preload the state for the Edit page.

2. Http verb to update should be PUT?

3. Add unit testing usign Jest and Enzyme

4. Design. Looks ugly I know.
